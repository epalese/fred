#!/bin/sh
# wait-for-postgres.sh

set -e

host="$FRED_DB_HOSTNAME"
db="postgres"
username="$FRED_DB_USERNAME"
password="$FRED_DB_PASSWORD"

until PGPASSWORD="$password" psql -h "$host" -U "$username" postgres -c '\q'; do
  >&2 echo "Postgres is not available"
  sleep 1
done

>&2 echo "Postgres is running"
