# FRED

## Prerequisites
You need the following:  
- [virtualenv](https://virtualenv.pypa.io/en/latest/)  
- [virtualenvwrapper](https://virtualenvwrapper.readthedocs.io/en/latest/)  
- [docker](https://docs.docker.com/)  
- [docker-compose](https://docs.docker.com/compose/install/)  


## Install
Clone this git repository:
```bash
$ git clone https://epalese@bitbucket.org/epalese/fred.git
```

## Usage
After cloning the repository, edit `docker-compose.yml` and `docker-compose-test.yml` and provide a valid
API key:
```yaml
...
  fred:
    build: .
    environment:
      - API_KEY=<use-your-api-key>
...
```  
Build the Docker containers and start them:
```bash
$ cd fred
fred$ docker-compose up --build
```

### Initial pipeline
In a separate terminal session, start a shell session on the `fred` container and run
the `fred_initial` pipeline.  
The initial pipeline will create all the database tables from scratch and gather the
full GDPC1, UMCSENT and UNRATE data series.  
Run the following commands:  
```bash
fred$ docker-compose exec fred /bin/bash
root@a24c59115799:/src# python fred/pipelines/fred_initial.py --debug
```  

When the pipeline terminates its execution, the data will be available in a Postgres database
named `fred`, running in the `postgres` docker-compose service.  

Note:  
The database can be accessed using a psql client directly from your local machine or from within
the docker-compose service `fred`.  

### Question: average rate of unemployment
We will use a psql client to run SQL queries to explore the data and answer the question:
> What was the average rate of unemployment for each year starting in 1980 and going up to 2015?

To answer this question we will use the UNRATE (Unemployment rate) data series.  
Using the same docker-compose shell session started before, run:  
```bash
root@a24c59115799:/src# PGPASSWORD=root psql -h postgres -U root fred
```

Let's explore the database and the `unrate` table.
To get a list of all available tables, run:
```bash
fred=# \d
             List of relations
 Schema |     Name      |   Type   | Owner 
--------+---------------+----------+-------
 public | errors        | table    | root
 public | errors_id_seq | sequence | root
 public | gdpc1         | table    | root
 public | gdpc1_tmp     | table    | root
 public | umcsent       | table    | root
 public | umcsent_tmp   | table    | root
 public | unrate        | table    | root
 public | unrate_tmp    | table    | root
(8 rows)
```  
Each data series is stored in a table named with the same name. There are also some auxiliary tables whose
purpose will be detailed in the following sections.  

The table that stores the `UNRATE` timeseries is `unrate`. To get a list of all the columns and a snapshot
of its content run:  
```bash
fred=# \d unrate
                          Table "public.unrate"
     Column     |            Type             |         Modifiers         
----------------+-----------------------------+---------------------------
 load_ts        | timestamp without time zone | default CURRENT_TIMESTAMP
 date           | date                        | not null
 realtime_start | date                        | not null
 realtime_end   | date                        | not null
 value          | double precision            | 
Indexes:
    "unrate_pkey" PRIMARY KEY, btree (date, realtime_start, realtime_end)

fred=# select * from unrate limit 3;
       load_ts       |    date    | realtime_start | realtime_end | value 
---------------------+------------+----------------+--------------+-------
 2018-12-19 07:28:25 | 1948-01-01 | 1960-03-15     | 1966-02-07   |   3.5
 2018-12-19 07:28:25 | 1948-01-01 | 1966-02-08     | 9999-12-31   |   3.4
 2018-12-19 07:28:25 | 1948-02-01 | 1960-03-15     | 1966-02-07   |   3.8
(3 rows)
```  
This is a description of the columns:  
- load_ts: timestamp of when the data has been loaded into the database  
- date: the date of the observation  
- realtime_start: the first date the value is valid  
- realtime_end: the last date the value is valid  

The real-time period marks when facts were true or when information was known until it changed (marked by 
realtime_end). 
The last available value is marked with `realtime_end =  '9999-12-31'`.  

The query to get the answer is the following:  
```sql
SELECT
	DATE_PART('year', date) as year,
	ROUND(AVG(value)::numeric, 1) as avg_unemployment
FROM unrate
WHERE DATE_PART('year', date) BETWEEN 1980 AND 2015
	AND realtime_end = '9999-12-31'
GROUP BY DATE_PART('year', date);
```  

In the psql session, execute the following statements:
```
fred=# SELECT DATE_PART('year', date) as year,
fred=# ROUND(AVG(value)::numeric, 1) as avg_unemployment
fred-# FROM unrate
fred-# WHERE DATE_PART('year', date) BETWEEN 1980 AND 2015
fred-# AND realtime_end = '9999-12-31'
fred-# GROUP BY DATE_PART('year', date);
------+------------------
 1980 |              7.2
 1981 |              7.6
 1982 |              9.7
 1983 |              9.6
 1984 |              7.5
 1985 |              7.2
 1986 |              7.0
 1987 |              6.2
 1988 |              5.5
 1989 |              5.3
 1990 |              5.6
 1991 |              6.9
 1992 |              7.5
 1993 |              6.9
 1994 |              6.1
 1995 |              5.6
 1996 |              5.4
 1997 |              4.9
 1998 |              4.5
 1999 |              4.2
 2000 |              4.0
 2001 |              4.7
 2002 |              5.8
 2003 |              6.0
 2004 |              5.5
 2005 |              5.1
 2006 |              4.6
 2007 |              4.6
 2008 |              5.8
 2009 |              9.3
 2010 |              9.6
 2011 |              8.9
 2012 |              8.1
 2013 |              7.4
 2014 |              6.2
 2015 |              5.3
(36 rows)
```
Note 1:  
The previous query considers only the rows where `realtime_end = '9999-12-31'`. This is because, as explained, 
the data exported by the pipelines include revisions. Specifying the above expression 
allow us to aggregate using only the most recent observations.  

Note 2:  
The correctness of the results can be verified using the FRED API itself. The API
provides a way to aggregate observations by different time frames. This query will provide
the avg rate of unemployment by year from 1980 to 2015:
```
https://api.stlouisfed.org/fred/series/observations?api_key=<use-your-api-key>&
file_type=json&series_id=UNRATE&frequency=a&aggregation_method=avg&
observation_start=1980-01-01&observation_end=2015-12-31
```

### Incremental pipeline
An incremental pipeline is also provided that allows to extract only a specified subset of dates for all three time series.  
The intended usage is to support iterative (e.g. daily, weekly) executions. It allows to specify a start
and an end date for the observations to be exported:
```bash
root@a24c59115799:/src# python fred/pipelines/fred_incremental.py --help
usage: fred_incremental.py [-h] [-s STARTDATE] [-e ENDDATE] [-d]

optional arguments:
  -h, --help            show this help message and exit
  -s STARTDATE, --startdate STARTDATE
                        Observation start date (YYYY-MM-DD). Yesterday date if
                        missing.
  -e ENDDATE, --enddate ENDDATE
                        Observation end date (YYYY-MM-DD). Current date if
                        omitted.
  -d, --debug           Increase logging level to DEBUG.
```
  
Note:  
The incremental pipeline works in a delete-before-append fashion: it will delete the records for the dates exported before inserting the new ones.  

Example:  
```
root@a24c59115799:/src# python fred/pipelines/fred_incremental.py --startdate 2018-01-01 --enddate 2018-10-01 --debug
DEBUG:root:Executing
        TRUNCATE TABLE gdpc1_tmp;

[truncate_gdpc1_tmp] SUCCESS: None
DEBUG:fred.pipelines.runner:Running task download_and_persist_gdpc1_tmp
DEBUG:fred:[download_and_persist_fred] error table = errors
DEBUG:fred:[download_and_persist_fred] observation_start_date = 2018-01-01
DEBUG:fred:[download_and_persist_fred] observation_end_date = 2018-10-01
...
```
  
## Run tests  
A test suite with unit and integration tests has been implemented. Even if the coverage is quite good, the test suite lacks of completeness. The goal of the tests provided is just to outline my test approach with respect to the data pipelines (and the code that supports their execution).  

### Run tests
Run:
```bash
$ cd fred
fred$ docker-compose -f docker-compose-test.yml up --build
...
fred_1      | Postgres is up - executing command
fred_1      | ============================= test session starts ==============================
fred_1      | platform linux -- Python 3.6.7, pytest-4.0.2, py-1.7.0, pluggy-0.8.0 -- /usr/local/bin/python
fred_1      | cachedir: .pytest_cache
fred_1      | rootdir: /src, inifile:
fred_1      | plugins: cov-2.6.0
fred_1      | collecting ... collected 23 items
fred_1      |
fred_1      | tests/integration/pipelines/test_int_fred_incremental.py::test_task_gdpc1 PASSED [  4%]
fred_1      | tests/integration/pipelines/test_int_fred_incremental.py::test_task_umcsent PASSED [  8%]
fred_1      | tests/integration/pipelines/test_int_fred_incremental.py::test_task_unrate PASSED [ 13%]
fred_1      | tests/integration/pipelines/test_int_fred_initial.py::test_task_setup_tables PASSED [ 17%]
fred_1      | tests/integration/pipelines/test_int_fred_initial.py::test_download_and_persist_gdpc1_task PASSED [ 21%]
fred_1      | tests/integration/pipelines/test_int_fred_initial.py::test_download_and_persist_umcsent_task PASSED [ 26%]
fred_1      | tests/integration/pipelines/test_int_fred_initial.py::test_download_and_persist_unrate_task PASSED [ 30%]
fred_1      | tests/unit/pipelines/test_fred_incremental.py::test_get_pipeline PASSED  [ 34%]
fred_1      | tests/unit/pipelines/test_fred_initial.py::test_get_pipeline PASSED      [ 39%]
fred_1      | tests/unit/pipelines/functions/test_common.py::test_download_and_persist_fred_gdpc1 PASSED [ 43%]
fred_1      | tests/unit/pipelines/functions/test_common.py::test_download_and_persist_fred_umcsent PASSED [ 47%]
fred_1      | tests/unit/pipelines/functions/test_common.py::test_download_and_persist_fred_unrate PASSED [ 52%]
fred_1      | tests/unit/pipelines/functions/test_common.py::test_parse_and_persist_fred_gdpc1 PASSED [ 56%]
fred_1      | tests/unit/pipelines/functions/test_common.py::test_parse_and_persist_fred_umcsent PASSED [ 60%]
fred_1      | tests/unit/pipelines/functions/test_common.py::test_parse_and_persist_fred_unrate PASSED [ 65%]
fred_1      | tests/unit/pipelines/functions/test_common.py::test_parse_and_persist_fred_with_errors PASSED [ 69%]
fred_1      | tests/unit/pipelines/functions/test_common.py::test_download_stream PASSED [ 73%]
fred_1      | tests/unit/pipelines/functions/test_common.py::test__handle_error PASSED [ 78%]
fred_1      | tests/unit/utils/test_sql.py::test_dbconnection_init_no_env PASSED       [ 82%]
fred_1      | tests/unit/utils/test_sql.py::test_dbconnection_init_with_env PASSED     [ 86%]
fred_1      | tests/unit/utils/test_sql.py::test_execute_sql PASSED                    [ 91%]
fred_1      | tests/unit/utils/test_sql.py::test_execute_sql_from_file PASSED          [ 95%]
fred_1      | tests/unit/utils/test_sql.py::test_execute_copy_from PASSED              [100%]
fred_1      |
fred_1      | ----------- coverage: platform linux, python 3.6.7-final-0 -----------
fred_1      | Name                                   Stmts   Miss  Cover
fred_1      | ----------------------------------------------------------
fred_1      | fred/__init__.py                           0      0   100%
fred_1      | fred/models/__init__.py                    0      0   100%
fred_1      | fred/models/schemas/__init__.py            0      0   100%
fred_1      | fred/models/schemas/observations.py       23      0   100%
fred_1      | fred/pipelines/__init__.py                 0      0   100%
fred_1      | fred/pipelines/fred_incremental.py        58     15    74%
fred_1      | fred/pipelines/fred_initial.py            42     10    76%
fred_1      | fred/pipelines/functions/__init__.py       0      0   100%
fred_1      | fred/pipelines/functions/common.py        51      1    98%
fred_1      | fred/utils/__init__.py                     0      0   100%
fred_1      | fred/utils/runner.py                      82     21    74%
fred_1      | fred/utils/sql.py                         39      1    97%
fred_1      | ----------------------------------------------------------
fred_1      | TOTAL                                    295     48    84%
fred_1      |
fred_1      |
fred_1      | ========================== 23 passed in 1.49 seconds ===========================
fred_fred_1 exited with code 0
```

## Design decisions
### API
FRED API uses the concept of vintage data to keep track of different revisions of the data series during the time.
Every observation can have three dates associated with it:
- date: the date the value is for  
- realtime_start: the first date the value is valid  
- realtime_end: the last date the value is valid  

The current value of an observation (i.e. the current revision) is identified by `realtime_end='9999-12-31'`.  
I have decided to export all the revisions since the exercise required to export the time series in their entirety even if the last revision would have been enough to answer the question.  

Also, the API directly provides aggregated values (i.e.: avg). This feature has been used to validate the
export and the query provided to compute the yearly average.

### Data model
All observations from the three different time series have the same structure:
```json
{
    "realtime_start":"2018-12-16",
    "realtime_end":"2018-12-16",
    "date":"1973-01-01",
    "value":"5642.669"
}
```
I have decided to use three different tables to store the data instead of a single table with a column to discriminate
between time series.
Using different tables helps in scenarios such as when the time series have different usage and aggregation patterns,
or have a big difference in cardinality, etc.

#### Schemas
The SQL DDL to create the tables are detailed in the following sections.  
I have used a composite primary key, built on `date, realtime_start, realtime_end`. This key ensures uniqueness and provides a built-in index for aggregations using these dates.
In case of data partitioning, the date field shows good properties as well (e.g.: it is very likely that we will
place data with the same dates on the same nodes).  

##### GDPC1
```sql
CREATE TABLE gdpc1 (
    load_ts TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    date DATE NOT NULL,
    realtime_start DATE NOT NULL,
    realtime_end DATE NOT NULL,
    value FLOAT,
    PRIMARY KEY (date, realtime_start, realtime_end)
)
;
```

##### UMCSENT
```sql
CREATE TABLE umcsent (
    load_ts TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    date DATE NOT NULL,
    realtime_start DATE NOT NULL,
    realtime_end DATE NOT NULL,
    value FLOAT,
    PRIMARY KEY (date, realtime_start, realtime_end)
)
;
```

##### UNRATE
```sql
CREATE TABLE unrate (
    load_ts TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    date DATE NOT NULL,
    realtime_start DATE NOT NULL,
    realtime_end DATE NOT NULL,
    value FLOAT,
    PRIMARY KEY (date, realtime_start, realtime_end)
)
;
```

##### Temporary tables
Each table has a temporary counterpart (identified by a _tmp_ suffix). The DDL used to create these temporary tables
is identical to the ones above.  
These temp tables are a sort of staging area used to hold the freshly exported data during an incremental pipeline before inserting it into the main tables.  

##### Error tables
An error table is also used whose role is to store malformed records coming from the FRED API.
The DDL of the error table is the following:  
```
CREATE TABLE IF NOT EXISTS errors (
    id serial NOT NULL PRIMARY KEY,
    record json NOT NULL
);
```  
The error table stores the record in its original JSON format.  


## Data Ingestion
### Paging and stream
I assumed to work in a scenario with a big amount of data to download. Under this scenario, it is important
to contain the memory footprint of the data pipeline during its execution.  

The approach used are:  
__Paging__
The API provides parameters like `limit` and `offset` that allows to navigate through the data.
The pipelines use these parameter to perform multiple small requests and limit the amount of data processed 
at each requests.  

__Stream__  
The data is downloaded in chunks (of 1024 bytes) and persisted.  
In our case, since each response from the API comes with an header, dealing with such a stream requires more 
effort (parse the stream while reading it, discard the header and identify each data record).  

The function [fred.pipelines.functions.commons.download_and_persist_fred] (https://bitbucket.org/epalese/fred/src/6ad73a3c6d2bf4ba57e7be5ff88dcb217d6e33ac/fred/pipelines/functions/common.py#lines-12)
provides a simplified (without parsing the bytestream on the fly) implementation of the above concepts.


### Validation
JSON data is validated using Marshmallow schemas (that could be found in [fred.models.schemas.observations.py](https://bitbucket.org/epalese/fred/src/6ad73a3c6d2bf4ba57e7be5ff88dcb217d6e33ac/fred/models/schemas/observations.py).

### Staging area
The same database is used to store staging tables and to host the actual tables with the time series.  
Pros:  
- no additional service required to support the staging area  
- no additional time required to load the data from a staging area into the database  

Cons:  
- the ETL load could affect analytical load  
- database space is usually more expensive than other kind of storage solution (e.g. S3, filesystem)  

### Side note: speed up ingestion
A possible solution to speed up the data ingestion process is to decouple the steps of reading from the API and persisting the data into the database (i.e. two threads that interact in a producer/consumer fashion).  
This could greatly enhance the throughput (even with Python and GIL) at the cost of an increased management complexity (e.g. have queues to to handle different throughputs between consumer and producer,
monitor the queues, etc.).  

## Data loading
The initial data pipeline loads the data directly into the master tables.  
The incremental pipeline performs the following steps:  
1) download the data (of the specified date range)  
2) store the data into temporary tables  
3) delete from master tables records with the specified date range  
4) insert the new data from temporary table into master table  

An important aspect to consider when using this approach is that deletion is generally an expensive operation. Sometimes it is preferable to recreate a new table performing two inserts instead of updating the same table with a delete followed by an insert.  

## ETL Pipeline design
This section outlines some design patterns followed during the implementation of the data pipelines.  

### Idempotency
Idempotency means that the pipeline produces the same result every time it runs with the same input parameters
(assuming that the data source doesn't change).  
Idempotency is important for the maintainability of the pipeline.
When tasks fail or their logic is changed it is important to ensure that it is safe to rerun the pipeline without
bringing the tables in a bad state.  

### Versioning 
It is important to being able to version data pipelines code and all related artefacts (DDL, SQL statements, configuration details).  
It is also important to track the evolution of the database during the time and possibly to use tools that help migrating between different versions of the databases (e.g. alembic).

### Resiliency
A data pipeline has be to robust and resilient to bad data and changes to the structure and format of input data.
When possible, in presence of malformed data, the data pipeline has to be robust enough to continue its operations while keeping track of bad data storing it in dead letter queues.  

## ETL Framework design
For this exercise I have decided to model a data pipeline implementing a very simple framework (instead of using an opensource solution such as Airflow or Luigi).  
The framework is all contained in [fred.utils.runner.py](https://bitbucket.org/epalese/fred/src/master/fred/utils/runner.py).
The main components are:  
- Tasks: that comes in different flavours (SQLTask, FunctionalTask)  
- Pipeline: a sequence of tasks  

The main ideas that underlie the framework are:
- data pipelines are purely Python based and they can be versioned  
- DDL to create database tables are stored in ad-hoc files: versioning and migration between different versions is easier  
- testability: each component of the pipeline (task) can be easily tested in isolation  

## Testing
### Unit testing
Unit tests are provided to test small part of the code in isolation (e.g. functions, classes).

### Integration testing
The integration tests provided test the interactions within the application (i.e. between the pipelines and the database). Interactions with external systems, like the FRED API, are not covered by integration tests.  
