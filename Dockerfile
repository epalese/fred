FROM python:3.6-slim-stretch

ENV PYTHONPATH /src
ARG DEBIAN_FRONTEND=noninteractive
ARG FRED_DB_HOSTNAME=localhost
ARG FRED_DB_NAME=test_fred
ARG FRED_DB_USERNAME=root
ARG FRED_DB_PASSWORD=root
ARG API_KEY=fakeapikey123


RUN mkdir -p /src/fred
WORKDIR /src
COPY requirements.txt .
COPY wait-for-postgres.sh .
RUN chmod +x ./wait-for-postgres.sh
RUN mkdir -p /usr/share/man/man1
RUN mkdir -p /usr/share/man/man7

RUN apt-get update && \
    apt-get install -y --no-install-recommends postgresql-client && \
    pip install --upgrade pip && pip install \
    --no-cache-dir -r requirements.txt

COPY fred ./fred/
COPY tests ./tests
