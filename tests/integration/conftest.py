import os
import pytest
import psycopg2

# #TODO: replace locahost with postgres
# os.environ['FRED_DB_HOSTNAME'] = 'localhost'
# os.environ['FRED_DB_NAME'] = 'test_fred'
# os.environ['FRED_DB_USERNAME'] = 'root'
# os.environ['FRED_DB_PASSWORD'] = 'root'
# os.environ['API_KEY'] = 'd6f2e40df10af234055b8d8b4fade945'


@pytest.fixture(scope='module')
def connection():
    connection = psycopg2.connect(
            host=os.getenv('FRED_DB_HOSTNAME'),
            database='postgres',
            user=os.getenv('FRED_DB_USERNAME'),
            password=os.getenv('FRED_DB_PASSWORD'))
    cursor = connection.cursor()
    connection.set_isolation_level(
        psycopg2.extensions.ISOLATION_LEVEL_AUTOCOMMIT)
    cursor.execute("""
        DROP DATABASE IF EXISTS {db};
    """.format(db=os.getenv('FRED_DB_NAME')))
    cursor.execute("""
        CREATE DATABASE {db};
    """.format(db=os.getenv('FRED_DB_NAME')))
    cursor.close()
    connection.set_isolation_level(psycopg2.extensions.ISOLATION_LEVEL_DEFAULT)
    connection = psycopg2.connect(
            host=os.getenv('FRED_DB_HOSTNAME'),
            database=os.getenv('FRED_DB_NAME'),
            user=os.getenv('FRED_DB_USERNAME'),
            password=os.getenv('FRED_DB_PASSWORD'))
    yield connection
    connection.commit()
    connection.close()
    # close all open sessions to the db
    # we have to change db otherwise our own
    # session will be killed
    connection = psycopg2.connect(
            host=os.getenv('FRED_DB_HOSTNAME'),
            database='postgres',
            user=os.getenv('FRED_DB_USERNAME'),
            password=os.getenv('FRED_DB_PASSWORD'))
    cursor = connection.cursor()
    cursor.execute("""
        select pg_terminate_backend(pid)
        from pg_stat_activity where datname='{}';
        """.format(os.getenv('FRED_DB_NAME')))


@pytest.fixture(scope='function')
def cursor(connection):
    cursor = connection.cursor()
    yield cursor
    connection.commit()
    cursor.close()


@pytest.fixture(scope='function')
def errors_tables(connection):
    sql_file = open('./fred/sql/ddl/errors.sql', 'r')
    stmt = sql_file.read()
    sql_file.close()
    cursor = connection.cursor()
    cursor.execute(stmt)
    connection.commit()
    cursor.close()


@pytest.fixture(scope='function')
def gdpc1_tables(connection):
    sql_file = open('./fred/sql/ddl/gdpc1.sql', 'r')
    stmt = sql_file.read()
    sql_file.close()
    cursor = connection.cursor()
    cursor.execute(stmt)
    connection.commit()
    cursor.close()


@pytest.fixture(scope='function')
def umcsent_tables(connection):
    sql_file = open('./fred/sql/ddl/umcsent.sql', 'r')
    stmt = sql_file.read()
    sql_file.close()
    cursor = connection.cursor()
    cursor.execute(stmt)
    connection.commit()
    cursor.close()


@pytest.fixture(scope='function')
def unrate_tables(connection):
    sql_file = open('./fred/sql/ddl/unrate.sql', 'r')
    stmt = sql_file.read()
    sql_file.close()
    cursor = connection.cursor()
    cursor.execute(stmt)
    connection.commit()
    cursor.close()


@pytest.fixture
def gdpc1_observations_stream_first5():
    return [
        b"""
        {
            "realtime_start": "1776-07-04",
            "realtime_end": "9999-12-31",
            "observation_start": "1600-01-01",
            "observation_end": "9999-12-31",
            "units": "lin",
            "output_type": 1,
            "file_type": "json",
            "order_by": "observation_date",
            "sort_order": "asc",
            "count": 10,
            "offset": 0,
            "limit": 5,
        """,
        b"""
        "observations": [
            {
                "realtime_start": "1992-12-22",
                "realtime_end": "1996-01-18",
                "date": "1947-01-01",
                "value": "1239.5"
            },
            {
                "realtime_start": "1996-01-19",
                "realtime_end": "1997-05-06",
                "date": "1947-01-01",
                "value": "."
            },
            {
                "realtime_start": "1997-05-07",
                "realtime_end": "1999-10-27",
                "date": "1947-01-01",
                "value": "1402.5"
            },
            {
                "realtime_start": "1999-10-28",
                "realtime_end": "2000-04-26",
                "date": "1947-01-01",
                "value": "."
            },
            {
                "realtime_start": "2000-04-27",
                "realtime_end": "2003-12-09",
                "date": "1947-01-01",
                "value": "1481.7"
            }
        ]
    }
    """
    ]


@pytest.fixture
def gdpc1_observations_stream_last5():
    return [
        b"""
        {
            "realtime_start": "1776-07-04",
            "realtime_end": "9999-12-31",
            "observation_start": "1600-01-01",
            "observation_end": "9999-12-31",
            "units": "lin",
            "output_type": 1,
            "file_type": "json",
            "order_by": "observation_date",
            "sort_order": "asc",
            "count": 10,
            "offset": 5,
            "limit": 5,
        """,
        b"""
        "observations": [
            {
                "realtime_start": "2003-12-10",
                "realtime_end": "2009-07-30",
                "date": "1947-01-01",
                "value": "1570.5"
            },
            {
                "realtime_start": "2009-07-31",
                "realtime_end": "2011-07-28",
                "date": "1947-01-01",
                "value": "1772.2"
            },
            {
                "realtime_start": "2011-07-29",
                "realtime_end": "2013-07-30",
                "date": "1947-01-01",
                "value": "1770.7"
            },
            {
                "realtime_start": "2013-07-31",
                "realtime_end": "2014-07-29",
                "date": "1947-01-01",
                "value": "1932.6"
            },
            {
                "realtime_start": "2014-07-30",
                "realtime_end": "2017-10-26",
                "date": "1947-01-01",
                "value": "1934.5"
            }
        ]
    }
    """
    ]


@pytest.fixture
def umcsent_observations_stream_first5():
    return [
        b"""
        {
            "realtime_start": "1776-07-04",
            "realtime_end": "9999-12-31",
            "observation_start": "1600-01-01",
            "observation_end": "9999-12-31",
            "units": "lin",
            "output_type": 1,
            "file_type": "json",
            "order_by": "observation_date",
            "sort_order": "asc",
            "count": 10,
            "offset": 0,
            "limit": 5,
        """,
        b"""
            "observations": [
                {
                    "realtime_start": "1998-07-31",
                    "realtime_end": "2003-12-22",
                    "date": "1952-11-01",
                    "value": "86.2"
                },
                {
                    "realtime_start": "2003-12-23",
                    "realtime_end": "2018-07-26",
                    "date": "1952-11-01",
                    "value": "."
                },
                {
                    "realtime_start": "2018-07-27",
                    "realtime_end": "9999-12-31",
                    "date": "1952-11-01",
                    "value": "86.2"
                },
                {
                    "realtime_start": "2018-07-27",
                    "realtime_end": "9999-12-31",
                    "date": "1952-12-01",
                    "value": "."
                },
                {
                    "realtime_start": "2018-07-27",
                    "realtime_end": "9999-12-31",
                    "date": "1953-01-01",
                    "value": "."
                }
            ]
        }
        """
    ]


@pytest.fixture
def umcsent_observations_stream_last5():
    return [
        b"""
        {
            "realtime_start": "1776-07-04",
            "realtime_end": "9999-12-31",
            "observation_start": "1600-01-01",
            "observation_end": "9999-12-31",
            "units": "lin",
            "output_type": 1,
            "file_type": "json",
            "order_by": "observation_date",
            "sort_order": "asc",
            "count": 10,
            "offset": 5,
            "limit": 5,
        """,
        b"""
            "observations": [
                {
                    "realtime_start": "1998-07-31",
                    "realtime_end": "2003-12-22",
                    "date": "1953-02-01",
                    "value": "90.7"
                },
                {
                    "realtime_start": "2003-12-23",
                    "realtime_end": "2018-07-26",
                    "date": "1953-02-01",
                    "value": "."
                },
                {
                    "realtime_start": "2018-07-27",
                    "realtime_end": "9999-12-31",
                    "date": "1953-02-01",
                    "value": "90.7"
                },
                {
                    "realtime_start": "2018-07-27",
                    "realtime_end": "9999-12-31",
                    "date": "1953-03-01",
                    "value": "."
                },
                {
                    "realtime_start": "2018-07-27",
                    "realtime_end": "9999-12-31",
                    "date": "1953-04-01",
                    "value": "."
                }
            ]
        }
        """
    ]


@pytest.fixture
def unrate_observations_stream_first5():
    return [
        b"""
        {
            "realtime_start": "1776-07-04",
            "realtime_end": "9999-12-31",
            "observation_start": "1600-01-01",
            "observation_end": "9999-12-31",
            "units": "lin",
            "output_type": 1,
            "file_type": "json",
            "order_by": "observation_date",
            "sort_order": "asc",
            "count": 10,
            "offset": 0,
            "limit": 5,
        """,
        b"""
            "observations": [
                {
                    "realtime_start": "1960-03-15",
                    "realtime_end": "1966-02-07",
                    "date": "1948-01-01",
                    "value": "3.5"
                },
                {
                    "realtime_start": "1966-02-08",
                    "realtime_end": "9999-12-31",
                    "date": "1948-01-01",
                    "value": "3.4"
                },
                {
                    "realtime_start": "1960-03-15",
                    "realtime_end": "1966-02-07",
                    "date": "1948-02-01",
                    "value": "3.8"
                },
                {
                    "realtime_start": "1966-02-08",
                    "realtime_end": "1967-02-08",
                    "date": "1948-02-01",
                    "value": "3.9"
                },
                {
                    "realtime_start": "1967-02-09",
                    "realtime_end": "9999-12-31",
                    "date": "1948-02-01",
                    "value": "3.8"
                }
            ]
        }
        """
    ]


@pytest.fixture
def unrate_observations_stream_last5():
    return [
        b"""
        {
            "realtime_start": "1776-07-04",
            "realtime_end": "9999-12-31",
            "observation_start": "1600-01-01",
            "observation_end": "9999-12-31",
            "units": "lin",
            "output_type": 1,
            "file_type": "json",
            "order_by": "observation_date",
            "sort_order": "asc",
            "count": 10,
            "offset": 5,
            "limit": 5,
        """,
        b"""
            "observations": [
                {
                    "realtime_start": "1960-03-15",
                    "realtime_end": "9999-12-31",
                    "date": "1948-03-01",
                    "value": "4.0"
                },
                {
                    "realtime_start": "1960-03-15",
                    "realtime_end": "1964-02-05",
                    "date": "1948-04-01",
                    "value": "4.0"
                },
                {
                    "realtime_start": "1964-02-06",
                    "realtime_end": "1966-02-07",
                    "date": "1948-04-01",
                    "value": "4.1"
                },
                {
                    "realtime_start": "1966-02-08",
                    "realtime_end": "1967-02-08",
                    "date": "1948-04-01",
                    "value": "4.0"
                },
                {
                    "realtime_start": "1967-02-09",
                    "realtime_end": "9999-12-31",
                    "date": "1948-04-01",
                    "value": "3.9"
                }
            ]
        }
        """
    ]
