import mock

import fred.pipelines.fred_incremental


@mock.patch('fred.pipelines.functions.common.download_stream')
def test_task_gdpc1(
    download_stream, cursor, gdpc1_tables,
    gdpc1_observations_stream_first5, gdpc1_observations_stream_last5
):
    # truncate gdpc1_tmp
    fred.pipelines.fred_incremental.t0.run()
    cursor.execute("select count(*) from gdpc1_tmp;")
    assert cursor.fetchone()[0] == 0

    # load gdpc1_tmp
    download_stream.side_effect = [
        gdpc1_observations_stream_first5,
        gdpc1_observations_stream_last5
    ]
    fred.pipelines.fred_incremental.t1.update_task_func_kwargs({'limit': 5})
    fred.pipelines.fred_incremental.t1.run()
    cursor.execute("select count(*) from gdpc1_tmp;")
    assert cursor.fetchone()[0] == 10

    # load gdpc1 (twice to test delete before append)
    fred.pipelines.fred_incremental.t2.update_properties({
        'observation_start_date': '1947-01-01',
        'observation_end_date': '1947-01-01'
    })
    fred.pipelines.fred_incremental.t2.run()
    fred.pipelines.fred_incremental.t2.run()
    cursor.execute("select count(*) from gdpc1;")
    assert cursor.fetchone()[0] == 10


@mock.patch('fred.pipelines.functions.common.download_stream')
def test_task_umcsent(
    download_stream, cursor, umcsent_tables,
    umcsent_observations_stream_first5, umcsent_observations_stream_last5
):
    # truncate umcsent_tmp
    fred.pipelines.fred_incremental.t3.run()
    cursor.execute("select count(*) from umcsent_tmp;")
    assert cursor.fetchone()[0] == 0

    # load umcsent_tmp
    download_stream.side_effect = [
        umcsent_observations_stream_first5,
        umcsent_observations_stream_last5
    ]
    fred.pipelines.fred_incremental.t4.update_task_func_kwargs({'limit': 5})
    fred.pipelines.fred_incremental.t4.run()
    cursor.execute("select count(*) from umcsent_tmp;")
    assert cursor.fetchone()[0] == 10

    # load umcsent (twice to test delete before append)
    fred.pipelines.fred_incremental.t5.update_properties({
        'observation_start_date': '1952-11-01',
        'observation_end_date': '1953-04-01'
    })
    fred.pipelines.fred_incremental.t5.run()
    fred.pipelines.fred_incremental.t5.run()
    cursor.execute("select count(*) from umcsent;")
    assert cursor.fetchone()[0] == 10


@mock.patch('fred.pipelines.functions.common.download_stream')
def test_task_unrate(
    download_stream, cursor, unrate_tables,
    unrate_observations_stream_first5, unrate_observations_stream_last5
):
    # truncate unrate_tmp
    fred.pipelines.fred_incremental.t6.run()
    cursor.execute("select count(*) from unrate_tmp;")
    assert cursor.fetchone()[0] == 0

    # load unrate_tmp
    download_stream.side_effect = [
        unrate_observations_stream_first5,
        unrate_observations_stream_last5
    ]
    fred.pipelines.fred_incremental.t7.update_task_func_kwargs({'limit': 5})
    fred.pipelines.fred_incremental.t7.run()
    cursor.execute("select count(*) from unrate_tmp;")
    assert cursor.fetchone()[0] == 10

    # load unrate (twice to test delete before append)
    fred.pipelines.fred_incremental.t8.update_properties({
        'observation_start_date': '1948-01-01',
        'observation_end_date': '1948-04-01'
    })
    fred.pipelines.fred_incremental.t8.run()
    fred.pipelines.fred_incremental.t8.run()
    cursor.execute("select count(*) from unrate;")
    assert cursor.fetchone()[0] == 10
