import mock

import fred.pipelines.fred_initial


def test_task_setup_tables(cursor):
    # gdpc1
    fred.pipelines.fred_initial.t0.run()
    cursor.execute("""
        select exists(
            select *
            from information_schema.tables
            where table_name=%s)
    """, ('gdpc1',))
    assert cursor.fetchone()[0]

    # umcsent
    fred.pipelines.fred_initial.t1.run()
    cursor.execute("""
        select exists(
            select *
            from information_schema.tables
            where table_name=%s)
    """, ('gdpc1',))
    assert cursor.fetchone()[0]

    # unrate
    fred.pipelines.fred_initial.t2.run()
    cursor.execute("""
        select exists(
            select *
            from information_schema.tables
            where table_name=%s)
    """, ('gdpc1',))
    assert cursor.fetchone()[0]


@mock.patch('fred.pipelines.functions.common.download_stream')
def test_download_and_persist_gdpc1_task(
    download_stream, cursor,
    gdpc1_observations_stream_first5,
    gdpc1_observations_stream_last5
):
    # run task that creates gdpc table
    fred.pipelines.fred_initial.t0.run()
    download_stream.side_effect = [
        gdpc1_observations_stream_first5,
        gdpc1_observations_stream_last5
    ]
    fred.pipelines.fred_initial.t4.update_task_func_kwargs({'limit': 5})
    fred.pipelines.fred_initial.t4.run()

    cursor.execute("select count(*) from gdpc1;")
    assert cursor.fetchone()[0] == 10

    cursor.execute("select count(*) from gdpc1 where value is NULL;")
    assert cursor.fetchone()[0] == 2


@mock.patch('fred.pipelines.functions.common.download_stream')
def test_download_and_persist_umcsent_task(
    download_stream, cursor,
    umcsent_observations_stream_first5,
    umcsent_observations_stream_last5
):
    # run task that creates umcsent table
    fred.pipelines.fred_initial.t1.run()
    download_stream.side_effect = [
        umcsent_observations_stream_first5,
        umcsent_observations_stream_last5
    ]
    fred.pipelines.fred_initial.t5.update_task_func_kwargs({'limit': 5})
    fred.pipelines.fred_initial.t5.run()

    cursor.execute("select count(*) from umcsent;")
    assert cursor.fetchone()[0] == 10

    cursor.execute("select count(*) from umcsent where value is NULL;")
    assert cursor.fetchone()[0] == 6


@mock.patch('fred.pipelines.functions.common.download_stream')
def test_download_and_persist_unrate_task(
    download_stream, cursor,
    unrate_observations_stream_first5,
    unrate_observations_stream_last5
):
    # run task that creates unrate table
    fred.pipelines.fred_initial.t2.run()
    download_stream.side_effect = [
        unrate_observations_stream_first5,
        unrate_observations_stream_last5
    ]
    fred.pipelines.fred_initial.t6.update_task_func_kwargs({'limit': 5})
    fred.pipelines.fred_initial.t6.run()

    cursor.execute("select count(*) from unrate;")
    assert cursor.fetchone()[0] == 10

    cursor.execute("select count(*) from unrate where value is NULL;")
    assert cursor.fetchone()[0] == 0