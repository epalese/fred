import mock
import pytest

import fred.utils.sql


@mock.patch('psycopg2.connect')
def test_dbconnection_init_no_env(connect):
    fred.utils.sql.FRED_DB_HOSTNAME = None
    fred.utils.sql.FRED_DB_NAME = None
    fred.utils.sql.FRED_DB_PASSWORD = None
    fred.utils.sql.FRED_DB_USERNAME = None

    with pytest.raises(ValueError):
        fred.utils.sql.DBConnection()

    with pytest.raises(ValueError):
        fred.utils.sql.DBConnection('hostname')

    dbc = fred.utils.sql.DBConnection('hostname', 'dbname', 'user', 'pwd')
    dbc.connect()

    assert connect.called is True
    connect.assert_called_with(
        host='hostname', database='dbname',
        user='user', password='pwd')


@mock.patch('psycopg2.connect')
def test_dbconnection_init_with_env(connect):
    fred.utils.sql.FRED_DB_HOSTNAME = 'hostname'
    fred.utils.sql.FRED_DB_NAME = 'dbname'
    fred.utils.sql.FRED_DB_USERNAME = 'user'
    fred.utils.sql.FRED_DB_PASSWORD = 'pwd'

    dbc = fred.utils.sql.DBConnection()
    dbc.connect()

    assert connect.called is True
    connect.assert_called_with(
        host='hostname', database='dbname',
        user='user', password='pwd')


@mock.patch('psycopg2.connect')
def test_execute_sql(connect):
    cursor = mock.Mock()
    connection = mock.Mock()
    connection.cursor.return_value = cursor
    connect.return_value = connection
    dbc = fred.utils.sql.DBConnection('hostname', 'dbname', 'user', 'pwd')
    dbc.connect()
    dbc.execute_sql("sql statement")
    assert connection.cursor.called is True
    assert cursor.execute.called is True
    assert cursor.close.called is True
    assert connection.commit.called is True


@mock.patch('psycopg2.connect')
@mock.patch("builtins.open")
def test_execute_sql_from_file(mock_open, connect):
    cursor = mock.Mock()
    connection = mock.Mock()
    connection.cursor.return_value = cursor
    connect.return_value = connection
    dbc = fred.utils.sql.DBConnection('hostname', 'dbname', 'user', 'pwd')
    dbc.connect()
    dbc.execute_sql_from_file("file.sql")
    mock_open.assert_called_with("file.sql", "r")
    assert connection.cursor.called is True
    assert cursor.execute.called is True
    assert cursor.close.called is True
    assert connection.commit.called is True


@mock.patch('psycopg2.connect')
def test_execute_copy_from(connect):
    cursor = mock.Mock()
    connection = mock.Mock()
    connection.cursor.return_value = cursor
    connect.return_value = connection
    dbc = fred.utils.sql.DBConnection('hostname', 'dbname', 'user', 'pwd')
    dbc.connect()
    dbc.copy_from(
        data='valcol1,valcol2,valcol3\nvalcol1,valcol2,valcol3',
        destination_table='destination',
        columns=['col1', 'col2', 'col3']
    )
    assert connection.cursor.called is True
    assert cursor.copy_from.called is True
    assert cursor.close.called is True
    assert connection.commit.called is True
