import fred.pipelines.fred_initial
from fred.utils.runner import StdOutNotificationHandler


def test_get_pipeline():
    notif_handler = StdOutNotificationHandler()
    pipeline = fred.pipelines.fred_initial.get_pipeline(notif_handler)
    assert fred.pipelines.fred_initial.t0 in pipeline.task_list
    assert fred.pipelines.fred_initial.t1 in pipeline.task_list
    assert fred.pipelines.fred_initial.t2 in pipeline.task_list
    assert fred.pipelines.fred_initial.t3 in pipeline.task_list
    assert fred.pipelines.fred_initial.t4 in pipeline.task_list
    assert fred.pipelines.fred_initial.t5 in pipeline.task_list
    assert fred.pipelines.fred_initial.t6 in pipeline.task_list
