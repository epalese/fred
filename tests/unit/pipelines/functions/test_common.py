import json

import mock

import fred.pipelines.functions.common
import fred.models.schemas.observations


@mock.patch('fred.pipelines.functions.common.download_stream')
@mock.patch('fred.pipelines.functions.common.parse_and_persist_fred')
def test_download_and_persist_fred_gdpc1(
    parse_and_persist_fred,
    download_stream,
    gdpc1_observations_stream_first5,
    gdpc1_observations_stream_last5
):
    # arguments used to call download_and_persist_fred
    db_connection = mock.Mock()
    source_url = 'https://test.org/fred/series/observations?'
    validator_class = fred.models.schemas.observations.GDPC1Schema
    destination_table = 'destination_table'
    error_table = 'error_table'
    observation_start_date = '2018-12-18'
    observation_end_date = '2018-12-18'

    download_stream.side_effect = [
        gdpc1_observations_stream_first5,
        gdpc1_observations_stream_last5
    ]

    fred.pipelines.functions.common.download_and_persist_fred(
        db_connection, source_url, validator_class,
        destination_table, error_table,
        observation_start_date, observation_end_date
    )

    assert len(download_stream.mock_calls) == 2
    first_url = download_stream.call_args_list[0][0][0]
    assert first_url == (
        source_url +
        '&observation_start=' + observation_start_date +
        '&observation_end=' + observation_end_date +
        '&limit=100000&offset=0'
    )
    second_url = download_stream.call_args_list[1][0][0]
    assert second_url == (
        source_url +
        '&observation_start=' + observation_start_date +
        '&observation_end=' + observation_end_date +
        '&limit=100000&offset=5'
    )
    assert len(parse_and_persist_fred.mock_calls) == 2


@mock.patch('fred.pipelines.functions.common.download_stream')
@mock.patch('fred.pipelines.functions.common.parse_and_persist_fred')
def test_download_and_persist_fred_umcsent(
    parse_and_persist_fred,
    download_stream,
    umcsent_observations_stream_first5,
    umcsent_observations_stream_last5
):
    # arguments used to call download_and_persist_fred
    db_connection = mock.Mock()
    source_url = 'https://test.org/fred/series/observations?'
    validator_class = fred.models.schemas.observations.UMCSENTSchema
    destination_table = 'destination_table'
    error_table = 'error_table'
    observation_start_date = '2018-12-18'
    observation_end_date = '2018-12-18'

    download_stream.side_effect = [
        umcsent_observations_stream_first5,
        umcsent_observations_stream_last5
    ]

    fred.pipelines.functions.common.download_and_persist_fred(
        db_connection, source_url, validator_class,
        destination_table, error_table,
        observation_start_date, observation_end_date
    )

    assert len(download_stream.mock_calls) == 2
    first_url = download_stream.call_args_list[0][0][0]
    assert first_url == (
        source_url +
        '&observation_start=' + observation_start_date +
        '&observation_end=' + observation_end_date +
        '&limit=100000&offset=0'
    )
    second_url = download_stream.call_args_list[1][0][0]
    assert second_url == (
        source_url +
        '&observation_start=' + observation_start_date +
        '&observation_end=' + observation_end_date +
        '&limit=100000&offset=5'
    )
    assert len(parse_and_persist_fred.mock_calls) == 2


@mock.patch('fred.pipelines.functions.common.download_stream')
@mock.patch('fred.pipelines.functions.common.parse_and_persist_fred')
def test_download_and_persist_fred_unrate(
    parse_and_persist_fred,
    download_stream,
    unrate_observations_stream_first5,
    unrate_observations_stream_last5
):
    # arguments used to call download_and_persist_fred
    db_connection = mock.Mock()
    source_url = 'https://test.org/fred/series/observations?'
    validator_class = fred.models.schemas.observations.UNRATESchema
    destination_table = 'destination_table'
    error_table = 'error_table'
    observation_start_date = '2018-12-18'
    observation_end_date = '2018-12-18'

    download_stream.side_effect = [
        unrate_observations_stream_first5,
        unrate_observations_stream_last5
    ]

    fred.pipelines.functions.common.download_and_persist_fred(
        db_connection, source_url, validator_class,
        destination_table, error_table,
        observation_start_date, observation_end_date)

    assert len(download_stream.mock_calls) == 2
    first_url = download_stream.call_args_list[0][0][0]
    assert first_url == (
        source_url +
        '&observation_start=' + observation_start_date +
        '&observation_end=' + observation_end_date +
        '&limit=100000&offset=0'
    )
    second_url = download_stream.call_args_list[1][0][0]
    assert second_url == (
        source_url +
        '&observation_start=' + observation_start_date +
        '&observation_end=' + observation_end_date +
        '&limit=100000&offset=5'
    )
    assert len(parse_and_persist_fred.mock_calls) == 2


@mock.patch('fred.pipelines.functions.common._handle_error')
def test_parse_and_persist_fred_gdpc1(
    handle_error,
    gdpc1_observations_stream_first5
):
    gdpc1_data = json.loads(
        ''.join([
            chunk.decode('utf-8')
            for chunk in gdpc1_observations_stream_first5
        ]))

    # arguments used to call parse_and_persist_fred
    db_connection = mock.Mock()
    observations = gdpc1_data['observations']
    validator = fred.models.schemas.observations.GDPC1Schema()
    destination_table = 'destination_table'
    error_table = 'error_table'
    load_ts = '2018-12-18 22:00:00'

    fred.pipelines.functions.common.parse_and_persist_fred(
        db_connection, observations, validator,
        destination_table, error_table, load_ts)

    assert db_connection.copy_from.called is True
    data_csv = (
        db_connection.copy_from.call_args_list[0][1]['data']
        .read().strip()
        .split('\n'))
    assert len(data_csv) == len(observations)
    # check only the first record
    assert data_csv[0].split(',')[0] == load_ts
    assert data_csv[0].split(',')[1] == observations[0]['date']
    assert data_csv[0].split(',')[2] == observations[0]['realtime_start']
    assert data_csv[0].split(',')[3] == observations[0]['realtime_end']
    assert data_csv[0].split(',')[4] == observations[0]['value']


@mock.patch('fred.pipelines.functions.common._handle_error')
def test_parse_and_persist_fred_umcsent(
    handle_error,
    umcsent_observations_stream_first5
):
    umcsent_data = json.loads(
        ''.join([
            chunk.decode('utf-8')
            for chunk in umcsent_observations_stream_first5
        ]))

    # arguments used to call parse_and_persist_fred
    db_connection = mock.Mock()
    observations = umcsent_data['observations']
    validator = fred.models.schemas.observations.UMCSENTSchema()
    destination_table = 'destination_table'
    error_table = 'error_table'
    load_ts = '2018-12-18 22:00:00'

    fred.pipelines.functions.common.parse_and_persist_fred(
        db_connection, observations, validator,
        destination_table, error_table, load_ts)

    assert db_connection.copy_from.called is True
    data_csv = (
        db_connection.copy_from.call_args_list[0][1]['data']
        .read().strip()
        .split('\n'))
    assert len(data_csv) == len(observations)
    # check only the first record
    assert data_csv[0].split(',')[0] == load_ts
    assert data_csv[0].split(',')[1] == observations[0]['date']
    assert data_csv[0].split(',')[2] == observations[0]['realtime_start']
    assert data_csv[0].split(',')[3] == observations[0]['realtime_end']
    assert data_csv[0].split(',')[4] == observations[0]['value']


@mock.patch('fred.pipelines.functions.common._handle_error')
def test_parse_and_persist_fred_unrate(
    handle_error,
    unrate_observations_stream_first5
):
    unrate_data = json.loads(
        ''.join([
            chunk.decode('utf-8')
            for chunk in unrate_observations_stream_first5
        ]))
    # arguments used to call parse_and_persist_fred
    db_connection = mock.Mock()
    observations = unrate_data['observations']
    validator = fred.models.schemas.observations.UNRATESchema()
    destination_table = 'destination_table'
    error_table = 'error_table'
    load_ts = '2018-12-18 22:00:00'

    fred.pipelines.functions.common.parse_and_persist_fred(
        db_connection, observations, validator,
        destination_table, error_table, load_ts)

    assert db_connection.copy_from.called is True
    data_csv = (
        db_connection.copy_from.call_args_list[0][1]['data']
        .read().strip()
        .split('\n'))
    assert len(data_csv) == len(observations)
    # check only the first record
    assert data_csv[0].split(',')[0] == load_ts
    assert data_csv[0].split(',')[1] == observations[0]['date']
    assert data_csv[0].split(',')[2] == observations[0]['realtime_start']
    assert data_csv[0].split(',')[3] == observations[0]['realtime_end']
    assert data_csv[0].split(',')[4] == observations[0]['value']


@mock.patch('fred.pipelines.functions.common._handle_error')
def test_parse_and_persist_fred_with_errors(handle_error):
    wrong_data = {
        'realtime_start': '1776-07-04',
        'realtime_end': '9999-12-31',
        'observation_start': '1600-01-01',
        'observation_end': '9999-12-31',
        'units': 'lin',
        'output_type': 1,
        'file_type': 'json',
        'order_by': 'observation_date',
        'sort_order': 'asc',
        'count': 10,
        'offset': 0,
        'limit': 5,
        'observations': [
            {
                'realtime_start': 'aaaaaaa',
                'realtime_end': '02-07-1996',
                'date': 12345,
                'value': 'not a number'
            },
            {
                'realtime_start': '1966-02-08',
                'realtime_end': '9999-12-31',
                'date': '1948-01-01',
                'value': '3.4'
            }
        ]
    }
    # arguments used to call parse_and_persist_fred
    db_connection = mock.Mock()
    observations = wrong_data['observations']
    validator = fred.models.schemas.observations.UNRATESchema()
    destination_table = 'destination_table'
    error_table = 'error_table'
    load_ts = '2018-12-18 22:00:00'

    fred.pipelines.functions.common.parse_and_persist_fred(
        db_connection, observations, validator,
        destination_table, error_table, load_ts)

    assert handle_error.called is True
    # get error dictionary
    errors = handle_error.call_args_list[0][0][2]
    assert errors['realtime_start'][0] == 'Not a valid date.'
    assert errors['date'][0] == 'Not a valid date.'
    assert errors['value'][0] == 'Not a valid number.'


@mock.patch('requests.get')
def test_download_stream(requests):
    iterator = mock.Mock()
    iterator.iter_content.return_value = ['data', 'data']
    requests.return_value.__enter__.return_value = iterator

    gen = fred.pipelines.functions.common.download_stream(
        source_url='source_url')
    downloaded_data = [chunk for chunk in gen]
    assert downloaded_data[0] == 'data'
    assert downloaded_data[1] == 'data'


def test__handle_error():
    # arguments used to call parse_and_persist_fred
    db_connection = mock.Mock()
    error_table = 'error_table'
    errors = {
        'date': ['Not a valid date.'],
        'value': ['Not a valid number.'],
        'realtime_start': ['Not a valid date.']
    }

    fred.pipelines.functions.common._handle_error(
        db_connection, error_table, errors
    )

    assert db_connection.execute_sql.called is True
    assert error_table in db_connection.execute_sql.call_args_list[0][0][0]
    assert str(errors) in db_connection.execute_sql.call_args_list[0][0][0]
