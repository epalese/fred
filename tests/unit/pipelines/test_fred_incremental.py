import fred.pipelines.fred_incremental
from fred.utils.runner import StdOutNotificationHandler


def test_get_pipeline():
    notif_handler = StdOutNotificationHandler()
    dates = {
        'observation_start_date': '2018-12-18',
        'observation_end_date': '2018-12-18'
    }
    pipeline = fred.pipelines.fred_incremental.get_pipeline(
        notif_handler, dates)
    assert fred.pipelines.fred_incremental.t0 in pipeline.task_list
    assert fred.pipelines.fred_incremental.t1 in pipeline.task_list
    assert fred.pipelines.fred_incremental.t2 in pipeline.task_list
    assert fred.pipelines.fred_incremental.t3 in pipeline.task_list
    assert fred.pipelines.fred_incremental.t4 in pipeline.task_list
    assert fred.pipelines.fred_incremental.t5 in pipeline.task_list
    assert fred.pipelines.fred_incremental.t6 in pipeline.task_list
    assert fred.pipelines.fred_incremental.t7 in pipeline.task_list
    assert fred.pipelines.fred_incremental.t8 in pipeline.task_list
