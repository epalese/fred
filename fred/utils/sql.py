import logging
import os

import psycopg2


logger = logging.getLogger('fred.utils.sql')

FRED_DB_HOSTNAME = os.getenv('FRED_DB_HOSTNAME')
FRED_DB_NAME = os.getenv('FRED_DB_NAME')
FRED_DB_USERNAME = os.getenv('FRED_DB_USERNAME')
FRED_DB_PASSWORD = os.getenv('FRED_DB_PASSWORD')


class DBConnection:
    """
    Create a connection instance to the Postgres database.
    Usage:
    >>> conn = DBConnection().connection
    >>> conn = DBConnection(
    ...     hostname='localhost', db_name='db',
    ...     username='postgres', password='postgres')

    If a parameter is not provided to the constructor, it
    will try to get the value from the following env variables:
    - FRED_DB_HOSTNAME
    - FRED_DB_NAME
    - FRED_DB_USERNAME
    - FRED_DB_PASSWORD
    """
    def __init__(
        self, hostname=None, db_name=None, username=None,
        password=None
    ):
        self.hostname = FRED_DB_HOSTNAME if FRED_DB_HOSTNAME else hostname
        self.db_name = FRED_DB_NAME if FRED_DB_NAME else db_name
        self.username = FRED_DB_USERNAME if FRED_DB_USERNAME else username
        self.password = FRED_DB_PASSWORD if FRED_DB_PASSWORD else password
        if (
            (self.hostname is None) or (self.db_name is None) or
            (self.username is None) or (self.password is None)
        ):
            raise ValueError(
                'Hostname, db name, username and password ' +
                'must be specified either as env variables ' +
                'or as arguments in the constructor.'
            )
        self.connection = None

    def __str__(self):
        return '<DBConnection: {}/{}>'.format(self.hostname, self.db_name)

    def connect(self):
        # check if already connected
        if self.connection and self.connection.closed == 0:
            return
        self.connection = psycopg2.connect(
            host=self.hostname, database=self.db_name,
            user=self.username, password=self.password
        )

    def execute_sql(self, sql_statement):
        cur = self.connection.cursor()
        logging.debug("Executing %s", sql_statement)
        cur.execute(sql_statement)
        cur.close()
        self.connection.commit()

    def execute_sql_from_file(self, filename):
        sql_file = open(filename, 'r')
        stmt = sql_file.read()
        sql_file.close()
        self.execute_sql(stmt)

    def copy_from(self, data, destination_table, columns, sep=',', null=''):
        """
        Perfom a bulk copy of `data` into destination table.
        The parameter `data` is a string where each line (separated by new
        line characters) contains a row of data with fields specified by
        `columns` parameter.
        """
        cur = self.connection.cursor()
        cur.copy_from(
            data, destination_table, columns=columns, sep=sep, null=null
        )
        self.connection.commit()
        cur.close()
