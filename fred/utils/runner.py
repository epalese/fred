import logging
from abc import ABC, abstractmethod

logger = logging.getLogger('fred.pipelines.runner')


class Task(ABC):
    """
    Defines an abstract task, an execution component that is
    part of a :class:`Pipeline`.
    A task must have a property `name` and must provide a
    `run` method.
    """
    @abstractmethod
    def run(self):
        pass

    @property
    @abstractmethod
    def name(self):
        pass

    def __eq__(self, other):
        if self.name == other.name:
            return True
        return False

    def __str__(self):
        return self.name


class FunctionalTask(Task):
    """
    Execute a parametric user-defined function.

    :param name: unique identifier of the task
    :type name: str
    :param task_func: user-defined function
    :type task_func: func
    :param task_func_args: positional arguments for task_func
    :type task_func_args: list
    :param task_func_kwargs: named arguments for task_func
    :type task_func_kwargs: dict
    """
    def __init__(
        self, name,
        task_func=None, task_func_args=[], task_func_kwargs={}
    ):
        self._name = name
        self.task_func = task_func
        self.task_func_args = task_func_args
        self.task_func_kwargs = task_func_kwargs

    @property
    def name(self):
        return self._name

    def update_task_func_kwargs(self, parameter_dict):
        if self.task_func_kwargs is not None:
            self.task_func_kwargs.update(parameter_dict)
        else:
            self.task_func_kwargs = {}
            self.task_func_kwargs.update(parameter_dict)

    def run(self):
        if self.task_func:
            logger.debug('Running task %s', self.name)
            return self.task_func(
                *self.task_func_args,
                **self.task_func_kwargs
            )
        logger.debug('Task %s has no function', self.name)


class SQLTask(Task):
    """
    Execute SQL statements. The statements can be stored
    in a string and/or in a file.

    :param name: unique identifier for the task
    :type name: str
    :param db_connection: connection to the database
    :type db_connection: :class:`fred.utils.sql.DBConnection`
    :param sql_statement: string with the SQL statements to execute
    :type sql_statement: str
    :param sql_file: file path containing the SQL statements
    :type sql_file: str
    :param properties: a dict mapping parameters inside SQL statements
        to values. The SQL statement can contain named parameters that
        are mapped to values using str.format().
    :type properties: dict
    """
    def __init__(
        self, name, db_connection,
        sql_statement=None, sql_file=None,
        properties=None
    ):
        self._name = name
        self.db_connection = db_connection
        self.sql_statement = sql_statement
        self.sql_file = sql_file
        if properties is None:
            self.properties = {}
        else:
            self.properties = properties

    @property
    def name(self):
        return self._name

    def update_properties(self, parameter_dict):
        self.properties.update(parameter_dict)

    def run(self):
        self.db_connection.connect()
        if self.sql_statement is not None:
            logger.debug(
                'Running task %s: SQL statement = %s',
                self.name,
                self.sql_statement.format(**self.properties)
            )
            self.db_connection.execute_sql(
                self.sql_statement.format(**self.properties)
            )
        if self.sql_file is not None:
            logger.debug(
                'Running task %s: SQL file = %s',
                self.name,
                self.sql_file
            )
            self.db_connection.execute_sql_from_file(self.sql_file)


class NotificationHandler(ABC):
    @abstractmethod
    def notify_success(self, task, return_value):
        pass

    @abstractmethod
    def notify_error(self, task, return_value):
        pass


class StdOutNotificationHandler(NotificationHandler):
    def notify_success(self, task, return_value):
        print("[{}] SUCCESS: {}".format(task, return_value))

    def notify_error(self, task, return_value):
        print("[{}] ERROR: {}".format(task, return_value))


class Pipeline:
    """
    A collection of tasks executed in sequence.

    :param name: unique identifier for the pipeline
    :type name: str
    :param notification_handler: instance of :class:`NotificationHandler`
        used to handle notifications of task execution.
    :type notification_handler: subtype of :class:`NotificationHandler`
    """

    def __init__(self, name, notification_handler):
        self.name = name
        self.task_list = []
        self.notification_handler = notification_handler

    def append(self, task):
        """
        Add a task to the to the execution pipeline.

        :param task: task to be added
        :type task: (sub)instance of :class:`Task`
        """
        self.task_list.append(task)
        return task

    def run(self):
        for task in self.task_list:
            try:
                return_value = task.run()
                if self.notification_handler:
                    self.notification_handler.notify_success(
                        task, return_value
                    )
            except Exception as e:
                if self.notification_handler:
                    self.notification_handler.notify_error(task, e)
                raise e
