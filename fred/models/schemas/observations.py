from marshmallow import Schema, fields


class DotAsNullMixin(object):
    def _deserialize(self, value, attr, obj):
        if value == '.':
            return ''
        return super()._deserialize(value, attr, obj)


class FloatWithNode(DotAsNullMixin, fields.Float):
    pass


class GDPC1Schema(Schema):
    date = fields.Date(required=True)
    realtime_start = fields.Date(required=True)
    realtime_end = fields.Date(required=True)
    value = FloatWithNode(required=True, allow_none=True)


class UMCSENTSchema(Schema):
    date = fields.Date(required=True)
    realtime_start = fields.Date(required=True)
    realtime_end = fields.Date(required=True)
    value = FloatWithNode(required=True, allow_none=True)


class UNRATESchema(Schema):
    date = fields.Date(required=True)
    realtime_start = fields.Date(required=True)
    realtime_end = fields.Date(required=True)
    value = FloatWithNode(required=True, allow_none=True)
