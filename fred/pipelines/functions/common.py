import datetime
import io
import json
import logging

import requests


logger = logging.getLogger('fred')


def download_and_persist_fred(
    db_connection, source_url, validator_class,
    destination_table, error_table=None,
    observation_start_date=None, observation_end_date=None,
    limit=100000
):
    """
    Download data from FRED API invoking the endpoint specified in
    `source_url` and stores it in `destination_table`.
    To validate JSON data coming from the FRED endpoint a `validator_class`
    must be specified.
    :param db_connection: connection to the database
    :type db_connection: :class:`fred.utils.sql.DBConnection`
    :param source_url: URL of the FRED endpoint
    :type source_url: str
    :param validator_class: subtype og marshmallow class defined in
        :py:mod:`fred.models.schemas.observations`
    :type validator_class: subtype of :class:`marshmallow.Schema`
    :param destination_table: target table where to store the data
    :type destination_table: str
    :param error_table: table where to store malformed records (according
    to the output of the validator)
    :type error_table: str
    :param observation_start_date: start date of the observations to fetch
    :type observation_start_date: str (with date format %Y-%m-%d)
    :param observation_end_date: end date of the observations to fetch
    :type observation_end_date: str (with date format %Y-%m-%d)
    :param limit: number of records to fetch for each invocation
    :type limit: int
    """
    load_ts = datetime.datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S')
    offset = 0
    observations_count = float('inf')
    validator = validator_class()
    logger.debug('[download_and_persist_fred] error table = %s', error_table)
    logger.debug(
        '[download_and_persist_fred] observation_start_date = %s',
        observation_start_date)
    logger.debug(
        '[download_and_persist_fred] observation_end_date = %s',
        observation_end_date)
    if observation_start_date:
        source_url += '&observation_start={}'.format(observation_start_date)
    if observation_end_date:
        source_url += '&observation_end={}'.format(observation_end_date)

    while offset < (observations_count - 1):
        current_souce_url = (
            source_url + '&limit={}&offset={}'.format(limit, offset))
        logging.debug('Requesting url: {}'.format(current_souce_url))
        stream = download_stream(current_souce_url)
        data_text = ''.join([chunk.decode('utf-8') for chunk in stream])
        if len(data_text) > 0:
            data_dict = json.loads(str(data_text))
            observations_count = data_dict['count']
            parse_and_persist_fred(
                db_connection, data_dict['observations'], validator,
                destination_table, error_table, load_ts
            )
        offset += len(data_dict['observations'])


def download_stream(source_url, chunk_size=1024):
    """
    Download data from the endpoint `source_url` as a stream of
    bytes by blocks of `chunk_size`.
    """
    logging.debug(source_url)
    headers = {
        'Content-Type': "application/json",
    }
    with requests.get(url=source_url, headers=headers, stream=True) as r:
        for chunk in r.iter_content(chunk_size=chunk_size):
            if chunk:
                yield chunk


def parse_and_persist_fred(
    db_connection, observations, validator,
    destination_table, error_table=None, load_ts=None
):
    """
    Validate the object in `observations` using a marshmallow
    model to validate and store the records into
    `destination_table`.
    `observations` has the following structure:
    observations = [
        {
            "realtime_start": "1992-12-22",
            "realtime_end": "1996-01-18",
            "date": "1947-01-01",
            "value": "1239.5"
        },
        ...
    ]

    :param db_connection: connection to the database
    :type db_connection: :class:`fred.utils.sql.DBConnection`
    :param observations: list of observations
    :type observations: list of dict
    :param validator: marshmallow schema defined in
        :py:mod:`fred.models.schemas.observations`
    :type validator: instance of class defined in
        :py:mod:`fred.models.schemas.observations`
    :param destination_table: target table where to store the data
    :type destination_table: str
    :param error_table: table where to store malformed records (according
    to the output of the validator)
    :type error_table: str
    :param load_ts: timestamp used to tag the current load
    'type load_ts': str (datetime format '%Y-%m-%d %H:%M:%S')

    """
    db_connection.connect()
    if load_ts is None:
        load_ts = datetime.datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S')
    data_csv = io.StringIO()
    for obs in observations:
        obs_obj = validator.load(obs)
        if (len(obs_obj.errors) > 0) and error_table is not None:
            _handle_error(db_connection, error_table, obs_obj.errors)
        else:
            row = (
                load_ts + ',' +
                obs_obj.data['date'].strftime('%Y-%m-%d') + ',' +
                obs_obj.data['realtime_start'].strftime('%Y-%m-%d') + ',' +
                obs_obj.data['realtime_end'].strftime('%Y-%m-%d') + ',' +
                str(obs_obj.data['value']) + '\n')
            data_csv.write(row)
    data_csv.seek(0)
    db_connection.copy_from(
        data=data_csv,
        destination_table=destination_table,
        columns=['load_ts', 'date', 'realtime_start', 'realtime_end', 'value']
    )


def _handle_error(db_connection, error_table, errors):
    logger.debug(
        "[parse_and_persist_fred] errors found while parsing: %s", errors)
    db_connection.execute_sql("""
        INSERT INTO {}
        VALUES (
            '{}'
        )
        """.format(error_table, errors)
    )