import argparse
import datetime
import logging
import os

from fred.models.schemas.observations import GDPC1Schema
from fred.models.schemas.observations import UMCSENTSchema
from fred.models.schemas.observations import UNRATESchema
from fred.pipelines.functions.common import download_and_persist_fred
from fred.utils.sql import DBConnection
from fred.utils.runner import FunctionalTask
from fred.utils.runner import Pipeline
from fred.utils.runner import StdOutNotificationHandler
from fred.utils.runner import SQLTask


API_KEY = os.getenv('API_KEY')
conn = DBConnection()


t0 = SQLTask(
    name='truncate_gdpc1_tmp',
    db_connection=conn,
    sql_statement="""
        TRUNCATE TABLE gdpc1_tmp;
    """
)

t1 = FunctionalTask(
    name='download_and_persist_gdpc1_tmp',
    task_func=download_and_persist_fred,
    task_func_kwargs={
        'db_connection': conn,
        'source_url': (
            'https://api.stlouisfed.org/fred/series/observations?' +
            'api_key={key}&file_type=json' +
            '&series_id=GDPC1' +
            '&realitime_start=1600-01-01&realtime_end=9999-12-31'
        ).format(key=API_KEY),
        'validator_class': GDPC1Schema,
        'destination_table': 'gdpc1_tmp',
        'error_table': 'errors'
    }
)

t2 = SQLTask(
    name='delete_and_append_gdpc1',
    db_connection=conn,
    sql_statement="""
        BEGIN;
        DELETE FROM gdpc1
        WHERE date >= '{observation_start_date}'
            AND date <= '{observation_end_date}';

        INSERT INTO gdpc1
        SELECT *
        FROM gdpc1_tmp
        WHERE date >= '{observation_start_date}'
            AND date <= '{observation_end_date}';
        COMMIT;
    """
)

t3 = SQLTask(
    name='truncate_umcsent_tmp',
    db_connection=conn,
    sql_statement="""
        TRUNCATE TABLE umcsent_tmp;
    """
)

t4 = FunctionalTask(
    name='download_and_persist_umcsent_tmp',
    task_func=download_and_persist_fred,
    task_func_kwargs={
        'db_connection': conn,
        'source_url': (
            'https://api.stlouisfed.org/fred/series/observations?' +
            'api_key={key}&file_type=json' +
            '&series_id=UMCSENT' +
            '&realitime_start=1600-01-01&realtime_end=9999-12-31'
        ).format(key=API_KEY),
        'validator_class': UMCSENTSchema,
        'destination_table': 'umcsent_tmp',
        'error_table': 'errors'
    }
)

t5 = SQLTask(
    name='delete_and_append_umcsent',
    db_connection=conn,
    sql_statement="""
        BEGIN;
        DELETE FROM umcsent
        WHERE date >= '{observation_start_date}'
            AND date <= '{observation_end_date}';

        INSERT INTO umcsent
        SELECT *
        FROM umcsent_tmp
        WHERE date >= '{observation_start_date}'
            AND date <= '{observation_end_date}';
        COMMIT;
    """
)

t6 = SQLTask(
    name='truncate_unrate_tmp',
    db_connection=conn,
    sql_statement="""
        TRUNCATE TABLE unrate_tmp;
    """
)

t7 = FunctionalTask(
    name='download_and_persist_unrate_tmp',
    task_func=download_and_persist_fred,
    task_func_kwargs={
        'db_connection': conn,
        'source_url': (
            'https://api.stlouisfed.org/fred/series/observations?' +
            'api_key={key}&file_type=json' +
            '&series_id=UNRATE' +
            '&realitime_start=1600-01-01&realtime_end=9999-12-31'
        ).format(key=API_KEY),
        'validator_class': UNRATESchema,
        'destination_table': 'unrate_tmp',
        'error_table': 'errors'
    }
)

t8 = SQLTask(
    name='delete_and_append_unrate',
    db_connection=conn,
    sql_statement="""
        BEGIN;
        DELETE FROM unrate
        WHERE date >= '{observation_start_date}'
            AND date <= '{observation_end_date}';

        INSERT INTO unrate
        SELECT *
        FROM unrate_tmp
        WHERE date >= '{observation_start_date}'
            AND date <= '{observation_end_date}';
        COMMIT;
    """
)


def get_pipeline(notification_handler, dates):
    # update tasks to extract data from start_date to end_date
    t1.update_task_func_kwargs(dates)
    t4.update_task_func_kwargs(dates)
    t7.update_task_func_kwargs(dates)
    # update tasks to delete data from start_date to end_date
    t2.update_properties(dates)
    t5.update_properties(dates)
    t8.update_properties(dates)

    pipeline = Pipeline(
        name='fred_incremental',
        notification_handler=notification_handler)
    pipeline.append(t0)
    pipeline.append(t1)
    pipeline.append(t2)
    pipeline.append(t3)
    pipeline.append(t4)
    pipeline.append(t5)
    pipeline.append(t6)
    pipeline.append(t7)
    pipeline.append(t8)
    return pipeline


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-s", "--startdate", type=str,
        help="Observation start date (YYYY-MM-DD). Yesterday date if missing."
    )
    parser.add_argument(
        "-e", "--enddate", type=str,
        help="Observation end date (YYYY-MM-DD). Current date if omitted."
    )
    parser.add_argument(
        "-d", "--debug", action="store_true",
        help="Increase logging level to DEBUG."
    )
    args = parser.parse_args()
    start_date = (
        args.startdate if args.startdate
        else (
            datetime.date.today() - datetime.timedelta(days=1)
        ).strftime('%Y-%m-%d')
    )
    end_date = (
        args.enddate if args.enddate
        else (
            datetime.date.today() - datetime.timedelta(days=1)
        ).strftime('%Y-%m-%d')
    )

    root_logger = logging.getLogger()
    if args.debug:
        root_logger.setLevel(logging.DEBUG)
    else:
        root_logger.setLevel(logging.ERROR)

    dates = {
        'observation_start_date': start_date,
        'observation_end_date': end_date
    }
    nh = StdOutNotificationHandler()

    pipeline = get_pipeline(nh, dates)
    pipeline.run()
