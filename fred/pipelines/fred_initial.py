import argparse
import logging
import os

from fred.models.schemas.observations import GDPC1Schema
from fred.models.schemas.observations import UMCSENTSchema
from fred.models.schemas.observations import UNRATESchema
from fred.pipelines.functions.common import download_and_persist_fred
from fred.utils.sql import DBConnection
from fred.utils.runner import FunctionalTask
from fred.utils.runner import Pipeline
from fred.utils.runner import StdOutNotificationHandler
from fred.utils.runner import SQLTask


API_KEY = os.getenv('API_KEY')
conn = DBConnection()


t0 = SQLTask(
    name='setup_gdpc1_tables',
    db_connection=conn,
    sql_file='./fred/sql/ddl/gdpc1.sql'
)

t1 = SQLTask(
    name='setup_umcsent_tables',
    db_connection=conn,
    sql_file='./fred/sql/ddl/umcsent.sql'
)

t2 = SQLTask(
    name='setup_unrate_tables',
    db_connection=conn,
    sql_file='./fred/sql/ddl/unrate.sql'
)

t3 = SQLTask(
    name='setup_errors_tables',
    db_connection=conn,
    sql_file='./fred/sql/ddl/errors.sql'
)

t4 = FunctionalTask(
    name='download_and_persist_GDPC1',
    task_func=download_and_persist_fred,
    task_func_kwargs={
        'db_connection': conn,
        'source_url': (
            'https://api.stlouisfed.org/fred/series/observations?' +
            'api_key={key}&file_type=json' +
            '&series_id=GDPC1' +
            '&realitime_start=1600-01-01&realtime_end=9999-12-31'
        ).format(key=API_KEY),
        'validator_class': GDPC1Schema,
        'destination_table': 'gdpc1',
        'error_table': 'errors'
    }
)

t5 = FunctionalTask(
    name='download_and_persist_UMCSENT',
    task_func=download_and_persist_fred,
    task_func_kwargs={
        'db_connection': conn,
        'source_url': (
            'https://api.stlouisfed.org/fred/series/observations?' +
            'api_key={key}&file_type=json' +
            '&series_id=UMCSENT' +
            '&realitime_start=1600-01-01&realtime_end=9999-12-31'
        ).format(key=API_KEY),
        'validator_class': UMCSENTSchema,
        'destination_table': 'umcsent',
        'error_table': 'errors'
    }
)

t6 = FunctionalTask(
    name='download_and_persist_UNRATE',
    task_func=download_and_persist_fred,
    task_func_kwargs={
        'db_connection': conn,
        'source_url': (
            'https://api.stlouisfed.org/fred/series/observations?' +
            'api_key={key}&file_type=json' +
            '&series_id=UNRATE' +
            '&realitime_start=1600-01-01&realtime_end=9999-12-31'
        ).format(key=API_KEY),
        'validator_class': UNRATESchema,
        'destination_table': 'unrate',
        'error_table': 'errors'
    }
)


def get_pipeline(notification_handler):
    pipeline = Pipeline(
        name='fred_initial',
        notification_handler=notification_handler)
    pipeline.append(t0)
    pipeline.append(t1)
    pipeline.append(t2)
    pipeline.append(t3)
    pipeline.append(t4)
    pipeline.append(t5)
    pipeline.append(t6)
    return pipeline


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-d", "--debug", action="store_true",
        help="Increase logging level to DEBUG."
    )
    args = parser.parse_args()

    root_logger = logging.getLogger()
    if args.debug:
        root_logger.setLevel(logging.DEBUG)
    else:
        root_logger.setLevel(logging.ERROR)

    stdout_nh = StdOutNotificationHandler()
    pipeline = get_pipeline(stdout_nh)
    pipeline.run()
