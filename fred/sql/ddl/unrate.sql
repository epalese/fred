DROP TABLE IF EXISTS unrate;
CREATE TABLE unrate (
    load_ts TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    date DATE NOT NULL,
    realtime_start DATE NOT NULL,
    realtime_end DATE NOT NULL,
    value FLOAT,
    PRIMARY KEY (date, realtime_start, realtime_end)
)
;

DROP TABLE IF EXISTS unrate_tmp;
CREATE TABLE unrate_tmp (
    load_ts TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    date DATE NOT NULL,
    realtime_start DATE NOT NULL,
    realtime_end DATE NOT NULL,
    value FLOAT,
    PRIMARY KEY (date, realtime_start, realtime_end)
)
;