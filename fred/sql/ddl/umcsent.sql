DROP TABLE IF EXISTS umcsent;
CREATE TABLE umcsent (
    load_ts TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    date DATE NOT NULL,
    realtime_start DATE NOT NULL,
    realtime_end DATE NOT NULL,
    value FLOAT,
    PRIMARY KEY (date, realtime_start, realtime_end)
)
;

DROP TABLE IF EXISTS umcsent_tmp;
CREATE TABLE umcsent_tmp (
    load_ts TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    date DATE NOT NULL,
    realtime_start DATE NOT NULL,
    realtime_end DATE NOT NULL,
    value FLOAT,
    PRIMARY KEY (date, realtime_start, realtime_end)
)
;