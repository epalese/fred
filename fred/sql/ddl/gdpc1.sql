DROP TABLE IF EXISTS gdpc1;
CREATE TABLE gdpc1 (
    load_ts TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    date DATE NOT NULL,
    realtime_start DATE NOT NULL,
    realtime_end DATE NOT NULL,
    value FLOAT,
    PRIMARY KEY (date, realtime_start, realtime_end)
)
;

DROP TABLE IF EXISTS gdpc1_tmp;
CREATE TABLE gdpc1_tmp (
    load_ts TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    date DATE NOT NULL,
    realtime_start DATE NOT NULL,
    realtime_end DATE NOT NULL,
    value FLOAT,
    PRIMARY KEY (date, realtime_start, realtime_end)
)
;
