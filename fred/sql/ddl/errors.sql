CREATE TABLE IF NOT EXISTS errors (
    id serial NOT NULL PRIMARY KEY,
    record json NOT NULL
);
